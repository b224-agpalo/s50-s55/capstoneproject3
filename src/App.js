import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import Home from './Pages/Home';
import AppNavbar from './Components/AppNavbar';
import Register from './Pages/Register';
import Login from './Pages/Login';
import { UserProvider } from './UserContext';
import Products from './Pages/Products';
import CourseView from './Pages/CourseView';
import AdminDashboard from './Pages/AdminDashboard'
import AddProducts from './Pages/AddProducts';
import UpdateProducts from './Pages/UpdateProducts'
import Logout from './Pages/Logout';
import Error from './Pages/Error';

function App() {
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  });


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        // Use is logged in
        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else { // User is logged out
          setUser({
            id: null,
            isAdmin: null
          })
        }

      })
  }, [])
  return (
    <UserProvider value={{ user, setUser, unsetUser }} >
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/admindashboard" element={<AdminDashboard />} />
            <Route path="/addproducts" element={<AddProducts />} />
            <Route path="/updateproducts" element={<UpdateProducts />} />



            <Route path="/logout" element={<Logout />} />
            <Route path="/*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>



  );
}

export default App;
