import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import CourseCard from '../Components/CourseCard';
import { Link } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import AllProductsTable from '../Components/AllProductsTable';



export default function AdminDashboard() {
    const [products, setProducts] = useState([]);
    const [allproducts, setAllProducts] = useState([]);


    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setProducts(data.map(products => {
                    return (
                        <CourseCard key={products._id} courseProp={products} />
                    )
                }))

            })
    }, [])

    return (
        <>
            <h1>Admin Dashboard</h1>
            <Button variant="primary" as={Link} to="/addproducts">Add New Product</Button>{' '}

            {products}
        </>
    )
}

