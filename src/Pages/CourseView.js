import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CourseView() {

    const { user, setUser } = useContext(UserContext);

    // Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling to a course.
    const navigate = useNavigate(); // useHistory

    // The "useParams" hook allows us to retrieve the courseId passed via the URL params.
    const { courseId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [status, setStatus] = useState("");


    const enroll = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {
                    Swal.fire({
                        title: "Successfully enrolled",
                        icon: "success",
                        text: "You have successfully enrolled for this course."
                    })

                    navigate("/admindashboard")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })
    };
    function updateCourse(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price

            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {

                    // Clear input fields
                    setName("");
                    setDescription("")
                    setPrice("");

                    Swal.fire({
                        title: "Registration Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })

                    navigate("/admindashboard");

                } else {

                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please, try again."
                    })
                }

            })

    }

    const archive = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${courseId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)


                if (data === true) {
                    Swal.fire({
                        title: "Successfully Archived",
                        icon: "success",
                        text: "You have successfully archived this course."
                    })

                    navigate("/admindashboard")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })
    };

    const activated = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/active/${courseId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)


                if (data === true) {
                    Swal.fire({
                        title: "Successfully Activated",
                        icon: "success",
                        text: "You have successfully activated this course."
                    })

                    navigate("/admindashboard")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })
    };



    useEffect(() => {

        console.log(courseId);

        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data)

                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setStatus(data.isActive);

            })


    }, [courseId])

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                // Global user state for validation accross the whole app
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    return (

        <Container>
            <Row>
                <Col lg={{ span: 6, offset: 3 }} >
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule:</Card.Subtitle>
                            <Card.Text>8:00 AM - 5:00 PM</Card.Text>

                            {
                                (user.id !== null && user.isAdmin === false) ?
                                    <Button variant="primary" onClick={() => enroll(courseId)} >Enroll</Button>
                                    :
                                    <><Button className="btn btn-danger" onClick={() => archive(courseId)} >Archive Course</Button>
                                        <Button className="btn btn-secondary" onClick={() => activated(courseId)} >Activate Course</Button>
                                    </>
                            }


                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            {(user.id !== null && user.isAdmin === false) ?
                <h1></h1>
                :
                <><Form onSubmit={(e) => updateCourse(e)}>
                    <h1>Update Products and Services</h1>

                    <Form.Group className="mb-3" controlId="name">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                            type="text"
                            value={name}
                            onChange={(e) => { setName(e.target.value) }}
                            placeholder="Product Name"
                            required />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="description">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                            type="text"
                            value={description}
                            onChange={(e) => { setDescription(e.target.value) }}
                            placeholder="Product Description" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="price">
                        <Form.Label>Product Price</Form.Label>
                        <Form.Control
                            type="number"
                            value={price}
                            onChange={(e) => { setPrice(e.target.value) }}
                            placeholder="Product Price" />
                    </Form.Group>

                    {
                        (user.id !== null && user.isAdmin === false) ?
                            <Button variant="primary" type="submit" id="submitBtn" disabled>
                                Update Product
                            </Button>
                            :
                            <Button variant="primary" type="submit" id="submitBtn">
                                Update Product
                            </Button>
                    }


                </Form>
                </>
            }


        </Container>



    )
}