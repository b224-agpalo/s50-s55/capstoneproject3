import Banner from "../Components/Banner"
//import CourseCard from '../components/CourseCard';
import Highlights from "../Components/Higlights"


export default function Home() {

    const data = {
        title: "PC Mabs Gapan",
        content: "PC repair for everyone, everywhere",
        destination: "/products",
        label: "Inquire now!"
    }


    return (

        <>
            <Banner data={data} />
            <Highlights />
            {/*<CourseCard/>*/}
        </>

    )
}