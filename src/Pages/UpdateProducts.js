import { Button, Form } from "react-bootstrap"
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';


export default function UpdateProducts() {
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");


    function addNewCourse(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/courses/addCourse`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price

            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {

                    // Clear input fields
                    setName("");
                    setDescription("")
                    setPrice("");

                    Swal.fire({
                        title: "Registration Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })

                    navigate("/admindashboard");

                } else {

                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please, try again."
                    })
                }

            })

    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                // Global user state for validation accross the whole app
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    console.log(name);
    console.log(description);
    console.log(price);


    return (
        <>


            <Form onSubmit={(e) => addNewCourse(e)}>
                <h1>Update Products and Services</h1>

                <Form.Group className="mb-3" controlId="name">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control
                        type="text"
                        value={name}
                        onChange={(e) => { setName(e.target.value) }}
                        placeholder="Product Name"
                        required />
                </Form.Group>
                <Form.Group className="mb-3" controlId="description">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Control
                        type="text"
                        value={description}
                        onChange={(e) => { setDescription(e.target.value) }}
                        placeholder="Product Description" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="price">
                    <Form.Label>Product Price</Form.Label>
                    <Form.Control
                        type="number"
                        value={price}
                        onChange={(e) => { setPrice(e.target.value) }}
                        placeholder="Product Price" />
                </Form.Group>

                <Button variant="primary" type="submit" id="submitBtn">
                    Add Product
                </Button>
            </Form>


        </>
    )
}

