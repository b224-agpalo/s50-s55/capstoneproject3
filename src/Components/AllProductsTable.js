import { Button, Card, Container } from "react-bootstrap";
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AllProductsTable({ productProp }) {
    const { user } = useContext(UserContext);
    const { _id, name, description, price, isActive } = productProp;
    const navigate = useNavigate();
    const { courseId } = useParams();



    const archive = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${courseId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {
                    Swal.fire({
                        title: "Successfully enrolled",
                        icon: "success",
                        text: "You have successfully enrolled for this course."
                    })

                    navigate("/courses")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })
    };
    const activate = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${courseId}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data === true) {
                    Swal.fire({
                        title: "Successfully enrolled",
                        icon: "success",
                        text: "You have successfully enrolled for this course."
                    })

                    navigate("/courses")

                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again."
                    })
                }

            })
    };
    useEffect(() => {

        console.log(courseId);

        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data)



            })


    }, [courseId])

    return (
        <tbody>
            <tr className="text-center text-dark table table-dark">
                <td><p>{name}</p></td>
                <td><p>{price}</p></td>
                <td><p>{description}</p></td>
                <td><p>{isActive}</p></td>
                <td>
                    <div className="d-block">
                        <button>Update</button>
                        {
                            (user.id !== null && user.isAdmin === false)
                                ?
                                <Button onClick={() => archive(courseId)} >Archive Course</Button>
                                :
                                <Button onClick={() => activate(courseId)} >archive Course</Button>
                        }
                    </div>

                </td>

            </tr>

        </tbody>





    )




}